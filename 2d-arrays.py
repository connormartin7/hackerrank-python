def hourglassSum(arr):
    list_of_sums = []
    width = len(arr[0])
    length = len(arr)
    for w in range(0, width - 2):
        for l in range(0, length - 2):
            top_left = arr[w][l]
            top_mid = arr[w][l + 1]
            top_right = arr[w][l + 2]
            mid_mid = arr[w + 1][l + 1]
            bot_left = arr[w + 2][l + 0]
            bot_mid = arr[w + 2][l + 1]
            bot_right = arr[w + 2][l + 2]
            list_of_sums.append(top_left+top_mid+top_right+mid_mid+bot_left+bot_mid+bot_right)
    return max(list_of_sums)
