def twoStrings(s1, s2):
    one_dict = defaultdict(int)
    for one in s1:
        one_dict[one] += 1
    for k in [key for key in one_dict.keys()]:
        if k in s2:
            print('YES')
            return 'YES'
    print('NO')
    return 'NO'
