def bubble_sort(arr):
    count = 0
    is_sorted = False
    length_sorted = len(arr) -1
    while is_sorted != True:
        is_sorted = True
        for i in range(0, length_sorted):
            if(arr[i] > arr[i+1]):
                arr[i], arr[i+1] = arr[i+1], arr[i]
                count += 1
                is_sorted = False
        length_sorted -= 1
    return arr
