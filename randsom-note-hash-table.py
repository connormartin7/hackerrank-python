def checkMagazine(magazine, note):
    mag_dict = defaultdict(int)
    for word in magazine:
        mag_dict[word] += 1
    for word in note:
        if word in mag_dict.keys() and mag_dict[word] != 0:
            mag_dict[word] -= 1
        else:
            print('No')
            return 'No'
    print('Yes')
    return 'Yes'
