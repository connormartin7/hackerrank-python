def selection_sort(arr):
    for i in range(0, len(arr)):
        min_position = i
        for j in range(i+1, len(arr)):
            if arr[min_position] > arr[j]:
                min_position = j
        arr[i], arr[min_position] = arr[min_position], arr[i]
    return arr
