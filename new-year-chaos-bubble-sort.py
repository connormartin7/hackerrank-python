def minimumBribes(q):
    swap = 0
    is_sorted = False
    last_unsorted = len(q) - 1
    for counter, person in enumerate(q):
        if ((person - 1) - counter) > 2:
            print('Too chaotic')
            return "Too chaotic"
    while is_sorted == False:
        is_sorted = True
        for i in range(0, last_unsorted):
            if (q[i] > q[i+1]):
                temp = q[i]
                q[i] = q[i+1]
                q[i+1] = temp
                swap += 1
                is_sorted = False
    last_unsorted -= 1 
    print(swap)
    return swap
